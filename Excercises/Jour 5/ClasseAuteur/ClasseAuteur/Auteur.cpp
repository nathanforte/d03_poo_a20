#include "Auteur.h"
#include <iostream>
Auteur::Auteur()
{
	strcpy_s(Auteur::nom, "Joe �crivain");
}

Auteur::Auteur(const char NOM[])
{
	strcpy_s(Auteur::nom, NOM);
}

const char * Auteur::getNom()
{
	return Auteur::nom;
}

const char * Auteur::getPrenom()
{
	return Auteur::prenom;
}

unsigned short Auteur::getPourcentage()
{
	return Auteur::pourcentage;
}

void Auteur::setNom(char nom[])
{
	strcpy_s(Auteur::nom, nom);
}

void Auteur::setPrenom(char prenom[])
{
	strcpy_s(Auteur::prenom, prenom);
}

void Auteur::setPourcentage(unsigned short pourcentage)
{
	Auteur::pourcentage = pourcentage;
}

void Auteur::afficher()
{
	std::cout << Auteur::nom << ", " << Auteur::prenom << ", " << Auteur::pourcentage << "%" << std::endl;
}
