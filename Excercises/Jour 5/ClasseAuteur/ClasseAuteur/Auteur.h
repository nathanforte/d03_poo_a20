#pragma once
class Auteur
{
	char nom[51];
	char prenom[51];
	unsigned short pourcentage;
public:
	Auteur();
	Auteur(const char NOM[]);
	const char* getNom();
	const char* getPrenom();
	unsigned short getPourcentage();
	void setNom(char nom[]);
	void setPrenom(char prenom[]);
	void setPourcentage(unsigned short pourcentage);
	void afficher();
};
