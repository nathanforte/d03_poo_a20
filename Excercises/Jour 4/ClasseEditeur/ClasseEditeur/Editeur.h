#pragma once

class Editeur {
private:
	char nom[76];
public:
	Editeur();
	Editeur(char nom[76]);
	char getNom();
	bool setNom(char nom[76]);
	void afficher();
};