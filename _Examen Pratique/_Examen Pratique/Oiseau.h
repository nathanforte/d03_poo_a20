#pragma once
#include "Animal.h"

class Oiseau : public Animal
{
public:
	Oiseau();
	Oiseau(unsigned short identifiant, char NOM, bool type);
	void voler();
};