#pragma once

class Animal
{
	unsigned short identifiant;
	char nom[51];
	bool type;
	bool vente;
public:
	Animal(unsigned short identifiant, char NOM, bool type);
	unsigned short getIdentifiant();
	const char* getNom();
	bool getType();
	bool getVente();
	void setVente();
	virtual void afficher();
	~Animal();
};