#pragma once
#include "Animal.h"

class Poisson: public Animal
{
	int longueur;
public:
	Poisson(unsigned short identifiant, char NOM, bool type);
	void afficher();
	void nager();
};