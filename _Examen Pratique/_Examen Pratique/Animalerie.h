#pragma once
#include "Animal.h"

class Animalerie
{
	int* tab[101];
	int nbAnimaux;
	void chercherAnimalDisponible();
public:
	Animalerie(); // Le seul constructeur demand�. Ne pas en ajouter d'autre.
	void ajouterUnAnimal();
	void vendreAnimal();
	void afficherTousLesAnimauxVendu();
	void faireBougerLesAnimaux();
	
};